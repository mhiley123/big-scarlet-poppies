A Pen created at CodePen.io. You can find this one at http://codepen.io/ncerminara/pen/xbGxVx.

 The last example only triggers the animation at the specified Scene trigger point. ScrollMagic can though bind your animation to the scroll event. This acts as a rewind and fast-forward scrubber for your animation.